module.exports = {
    "rules": {
        "indent": [
            2,
            4
        ],
        "linebreak-style": [
            0,
            "unix"
        ],
        "semi": [
            2,
            "always"
        ],
        "no-unused-vars": 1,
        "no-console": 1
    },
    "env": {
        "node": true,
        "es6": true
    },
    "extends": "eslint:recommended"
};