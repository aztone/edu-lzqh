module.exports = {
    "rules": {
        "indent": [
            2,
            4
        ],
        "linebreak-style": [
            0,
            "unix"
        ],
        "semi": [
            2,
            "always"
        ],
        "no-unused-vars": 1,
        "no-console": 1
    },
    "env": {
        "browser": true,
        "es6": false
    },
    "globals": {
        "angular": true,
        "$": true,
        "moment": true,
        "_": true,
        "go": true,
        "js_beautify": true,
        "Promise": true,
        "io": true,
    },
    "extends": "eslint:recommended"
};