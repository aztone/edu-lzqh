{
    "header" : {
        "navbar" : {
            "new" : {
                "new" : "新建",
                "project" : "项目"
            }
        }
    },
    "aside" : {
        "nav" : {
            "header" : "导航",
            "dashboard" : "总览",
            "menu": "菜单",
            "menu1": "菜单1",
            "func1": "功能1",
            "menu2": "菜单2",
            "func2": "功能2",
            "stuff": "资料",
            "profile": "个人信息",
            "document": "文档",
            "stat": {
                "assign": "当天派单",
                "handle": "当天处理",
                "complete":"当天完成"
            }
        }
    }
}
