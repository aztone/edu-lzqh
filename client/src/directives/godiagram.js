'use strict';
var app = angular.module('app');
app.directive('goDiagram', function() {
    return {
        restrict: 'E',
        template: '<div></div>',  
        replace: true,
        scope: { 
            model: '=goModel'        
        },
        link: function(scope, element, attrs) {
            var $ = go.GraphObject.make;
            var nodeTemplate = $(go.Node, "Auto",
                new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                $(go.Shape, "RoundedRectangle", {
                    parameter1: 5,  // the corner has a large radius
                    fill: $(go.Brush, "Linear", { 0: "rgb(254, 201, 0)", 1: "rgb(254, 162, 0)" }),
                    stroke: "black",
                    portId: "",
                    fromLinkable: true,
                    fromLinkableSelfNode: true,
                    fromLinkableDuplicates: true,
                    toLinkable: true,
                    toLinkableSelfNode: true,
                    toLinkableDuplicates: true,
                    cursor: "pointer"
                }),
                $(go.TextBlock, {
                    font: "bold 11pt helvetica, bold arial, sans-serif",
                    editable: true
                },
                new go.Binding("text", "text").makeTwoWay())
            );
            nodeTemplate.selectionAdornmentTemplate = $(go.Adornment, "Spot",
                $(go.Panel, "Auto", $(go.Shape, { fill: null, stroke: "blue", strokeWidth: 2 }), $(go.Placeholder)),
                $("Button",{alignment: go.Spot.TopRight,
                    click: addNodeAndLink}, 
                    $(go.Shape, "PlusLine", { desiredSize: new go.Size(6, 6) })
                ) 
            ); 

            var linkTemplate = $(go.Link, 
                    {curve: go.Link.Bezier, adjusting: go.Link.Stretch,
                        reshapable: true, relinkableFrom: true, relinkableTo: true},
                    new go.Binding("points").makeTwoWay(),
                    new go.Binding("curviness", "curviness"),
                    $(go.Shape, { strokeWidth: 1.5 }), $(go.Shape, { toArrow: "standard", stroke: null }),
                    $(go.Panel, "Auto",
                    $(go.Shape, {
                        fill: $(go.Brush, "Radial", { 0: "rgb(240, 240, 240)", 0.3: "rgb(240, 240, 240)", 1: "rgba(240, 240, 240, 0)" }), stroke: null }),
                        $(go.TextBlock, "默认", {
                            textAlign: "center",
                            font: "10pt helvetica, arial, sans-serif",
                            stroke: "black",
                            margin: 4,
                            editable: true
                        },
                        new go.Binding("text", "text").makeTwoWay())
                    )
            );

            var diagram = $(go.Diagram, element[0], {
                nodeTemplate: nodeTemplate,
                linkTemplate: linkTemplate,
                initialContentAlignment: go.Spot.Center,
                "ModelChanged": updateAngular,
                "toolManager.mouseWheelBehavior": go.ToolManager.WheelZoom,
                "undoManager.isEnabled": true
            });

            function updateAngular(e) {
                if (e.isTransactionFinished) scope.$apply();
            }

            scope.$watch("model", function(newmodel) {                
                var oldmodel = diagram.model;
                if (oldmodel !== newmodel) {
                    diagram.removeDiagramListener("ChangedSelection", change);
                    diagram.model = newmodel;                    
                    diagram.addDiagramListener("ChangedSelection", change);
                }
            });

            function change(e) {
                var selnode = diagram.selection.first();
                scope.model.selectedNodeData = (selnode instanceof go.Node ? selnode.data : null);               
                scope.$apply();
            }
            
            scope.$watch("model.selectedNodeData.name", function(newname) {
                diagram.removeModelChangedListener(updateAngular);
                diagram.startTransaction("change name");
                var node = diagram.findNodeForData(diagram.model.selectedNodeData);
                if (node !== null) node.updateTargetBindings("name");
                diagram.commitTransaction("change name");
                diagram.addModelChangedListener(updateAngular);
            });      
            
            function addNodeAndLink(e, obj) {
                var adorn = obj.part;
                e.handled = true;
                var diagram = adorn.diagram;
                diagram.startTransaction("Add State");

                // get the node data for which the user clicked the button
                var fromNode = adorn.adornedPart;
                var fromData = fromNode.data;
                // create a new "State" data object, positioned off to the right of the adorned Node
                var toData = { text: "新节点" };
                var p = fromNode.location.copy();
                p.x += 200;
                toData.loc = go.Point.stringify(p);  // the "loc" property is a string, not a Point object
                // add the new node data to the model
                var model = diagram.model;
                model.addNodeData(toData);

                // create a link data from the old node data to the new node data
                var linkdata = {
                    from: model.getKeyForNodeData(fromData),  // or just: fromData.id
                    to: model.getKeyForNodeData(toData),
                    text: "默认"
                };
                // and add the link data to the model
                model.addLinkData(linkdata);

                // select the new Node
                var newnode = diagram.findNodeForData(toData);
                diagram.select(newnode);

                diagram.commitTransaction("Add State");

                // if the new node is off-screen, scroll the diagram to show the new node
                diagram.scrollToRect(newnode.actualBounds);
            }
        }
    };
});