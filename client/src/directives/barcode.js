'use strict';

angular.module('app').directive('barcode', function ($timeout) {
    return {
        restrict: 'AE',
        scope: {
            data: '@'
        },
        link: function (scope, element, attrs) {
            var settings = {
                barWidth: 2,
                barHeight: 50
            };
            scope.$watch('data', function () {
                $(element).barcode(scope.data, 'code128', settings);
            });
        }
    }
});
