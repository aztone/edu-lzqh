/**
 * @fileOverview angular指令集
 * @author wing ying_gong@bjdv.com
 */
'use strict';
var app = angular.module('app');

/** 部门 */
app.directive('department', function () {
    return {
        restrict: 'AE',
        scope: {
            department: '@'
        },
        template: "<span>{{data.name}}</span>",
        controller: function ($scope, $department) {
            $scope.$watch('department', function () {
                if ($scope.department) {
                    $department.findOne($scope.department).then(function (data) {
                        $scope.data = data;
                    });
                }
            });
        }
    };
});

/** 名称翻译 */
app.directive('transContext', function (GlobalMetaService, MetadataService) {
    return {
        restrict: 'AE',
        scope: {
            transVal: '@',
            transContext: '@',
            type: '@',//G-global_meta M-metadata
        },
        template: "{{name}}",
        controller: function ($scope) {
            if (!_.isUndefined($scope.transVal)) {
                if ($scope.type == 'M') {
                    MetadataService.trans($scope.transContext, $scope.transVal).then(function (r) {
                        $scope.name = r;
                    }).catch(function () {
                        $scope.name = "";
                    });
                } else {
                    GlobalMetaService.trans($scope.transContext, $scope.transVal).then(function (r) {
                        $scope.name = r;
                    }).catch(function () {
                        $scope.name = "";
                    });

                }
            }
        }
    };
});

/** 状态翻译 */
app.directive('transStatus', function (GlobalMetaService) {
    return {
        restrict: 'AE',
        scope: {
            transStatus: '@'
        },
        template: "{{name}}",
        controller: function ($scope) {
            if (!_.isUndefined($scope.transStatus)) {
                GlobalMetaService.trans('status', $scope.transStatus).then(function (r) {
                    $scope.name = r;
                }).catch(function () {
                    $scope.name = "";
                });
            }
        }
    };
});

/**
 * 队列策略
 */
app.directive('transStrategy', function (GlobalMetaService) {
    return {
        restrict: 'AE',
        scope: {
            transStrategy: '@'
        },
        template: "{{name}}",
        controller: function ($scope) {
            if (!_.isUndefined($scope.transStrategy)) {
                GlobalMetaService.trans('queue:strategy', $scope.transStrategy).then(function (r) {
                    $scope.name = r;
                }).catch(function () {
                    $scope.name = "";
                });
            }
        }
    };
});

/** 
 * 坐席翻译
 */
app.directive('transUser', function () {
    return {
        restrict: 'AE',
        scope: {
            transUser: '@'
        },
        template: "{{name}}",
        controller: function ($scope, $user) {
            if (!_.isUndefined($scope.transUser)) {
                $user.findOne($scope.transUser, ['name']).then(function (r) {
                    $scope.name = r.name;
                }).catch(function () {
                    $scope.name = "";
                });
            }
        }
    };
});

app.directive('transLevel', function () {
    return {
        restrict: 'AE',
        scope: {
            transLevel: '@'
        },
        template: "{{name}}",
        controller: function ($scope, $level) {
            if (!_.isUndefined($scope.transLevel)) {
                $level.findAll({ level: $scope.transLevel }, [], ['name']).then(function (rows) {
                    if (rows.length > 0) {
                        $scope.name = rows[0].name;
                    }
                }).catch(function () {
                    $scope.name = "";
                });
            }
        }
    };
});

app.directive('transType', function () {
    return {
        restrict: 'AE',
        scope: {
            transType: '@'
        },
        template: "{{name}}",
        controller: function ($scope, GlobalMetaService) {
            if (!_.isUndefined($scope.transType)) {
                GlobalMetaService.trans('cti:type', $scope.transType).then(function (r) {
                    $scope.name = r;
                }).catch(function () {
                    $scope.name = "";
                });
            }
        }
    };
});

app.directive('transTarget', function () {
    return {
        restrict: 'AE',
        scope: {
            targetType: '@',
            transTarget: '@'
        },
        template: "{{name}}",
        controller: function ($scope, $agent, $ivr, $queue) {
            if (!_.isUndefined($scope.transTarget && !_.isUndefined($scope.targetType))) {
                Promise.resolve().then(function () {
                    if ($scope.targetType == '3') {
                        return $queue.findOne($scope.transTarget, ['name']);
                    } else if ($scope.targetType == '4') {
                        return $ivr.findOne($scope.transTarget, ['name']);
                    } else if ($scope.targetType == '5') {
                        return $agent.findOne($scope.transTarget, ['name']);
                    }
                }).then(function (row) {
                    if ($scope.targetType == '1') {
                        $scope.name = $scope.transTarget;
                    } else
                        $scope.name = row.name;
                }).catch(function () {
                    $scope.name = "";
                });
            }
        }
    };
});

/**
 * 地区联动
 */
app.directive('areaStreet', function () {
    return {
        restrict: 'AE',
        scope: {
            areaStreet: '='
        },
        templateUrl: 'views/blocks/street.html',
        controller: function ($scope, $area_street) {
            $scope.$watch('::areaStreet', function () {
                if (!$scope.areaStreet || $scope.areaStreet.length < 2) {
                    $scope.province = "";
                }
                if ($scope.areaStreet && $scope.areaStreet.length >= 2) {
                    $scope.province = $scope.areaStreet.substr(0, 2);
                }
                if ($scope.areaStreet && $scope.areaStreet.length >= 4) {
                    $scope.city = $scope.areaStreet.substr(0, 4);
                }
                if ($scope.areaStreet && $scope.areaStreet.length >= 6) {
                    $scope.area = $scope.areaStreet.substr(0, 6);
                }
                if ($scope.areaStreet && $scope.areaStreet.length >= 9) {
                    $scope.street = $scope.areaStreet.substr(0, 9);
                }
                $scope.fill();
            });

            $scope.provinceChange = function () {
                $scope.areaStreet = $scope.province;
                $scope.city = "";
                $scope.cities = [];
                $scope.area = "";
                $scope.areas = [];
                $scope.street = "";
                $scope.streets = [];
                $area_street.findAll($scope.areaStreet).then(function (rows) {
                    $scope.cities = rows;
                });
            };

            $scope.cityChange = function () {
                $scope.areaStreet = $scope.city;
                $scope.area = "";
                $scope.areas = [];
                $scope.street = "";
                $scope.streets = [];
                $area_street.findAll($scope.areaStreet).then(function (rows) {
                    $scope.areas = rows;
                });
            };

            $scope.areaChange = function () {
                $scope.areaStreet = $scope.area;
                $scope.street = "";
                $scope.streets = [];
                $area_street.findAll($scope.areaStreet).then(function (rows) {
                    $scope.streets = rows;
                });
            };

            $scope.streetChange = function () {
                $scope.areaStreet = $scope.street;
            };

            $scope.fill = function () {
                $area_street.findAll("").then(function (rows) {
                    $scope.provinces = rows;
                });
                if ($scope.province) {
                    $area_street.findAll($scope.province).then(function (rows) {
                        $scope.cities = rows;
                    });
                }
                if ($scope.city) {
                    $area_street.findAll($scope.city).then(function (rows) {
                        $scope.areas = rows;
                    });
                }
                if ($scope.area) {
                    $area_street.findAll($scope.area).then(function (rows) {
                        $scope.streets = rows;
                    });
                }
            };
        }
    };
});

/**
 * 部门树（带文件夹图标）
 */
app.directive('depsTree', function () {
    return {
        restrict: 'AE',
        scope: {
            pRanges: '@',//可选范围的部门树
            pChange: '=',//可选择范围时 调用指令页面的controller中的方法（执行完指令的回调方法）
            pDepartments: '=',//要展现的部门列表
            pDepusers: '=',//部门对应的员工
            pProperty: '@',//老师2、学生4
            pChecked: '@',//是否有复选框
            pSeloneclass: '@',//true只能选择班级并只能选择一个班级 此参数有值时pChecked可以不传值
            pShowusers: '@',//树结构内是否显示员工
            pLimit: "@",//限制查询员工数量 true
            pTotal: "=",
            pTpage: "=",
            pSelectedp: "="//选择的部门
        },
        templateUrl: 'views/blocks/deps_tree.html',
        controller: function ($scope, $timeout, UserService, $myhttp, $department, $user) {
            $scope.opera = function (type) {
                $scope.pChange(type);
            };

            $scope.getData = function (node, d) {
                var where = { department_id: { $like: '%#' + node.id + '#%' }, status: 1 };
                var page = undefined;
                var limit = undefined;
                if ($scope.pLimit) {
                    page = 1;
                    limit = 10;
                }
                if ($scope.pProperty) {//点击部门  员工类型
                    $scope.pSelectedp = node;
                    where.property = parseInt($scope.pProperty);
                } else
                    where.property = 2;
                console.log(where);
                if (d && !$scope.pSeloneclass) {//点击部门名字
                    //$myhttp.get('/api/user', {where: where, page: page, limit: limit}).then(function(users){
                    $user.findAllPage(where, page, limit).then(function (users) {
                        if ($scope.pLimit) {
                            $scope.pDepusers = users.rows;
                            $scope.pTotal = users.count;
                            $scope.pTpage = users.total;
                        } else
                            $scope.pDepusers = users;
                    });
                } else {//点击右侧展开收缩标签
                    if (!node.collapsed) {//展
                        node.collapsed = true;
                        if (!node.nodes || node.nodes.length <= 0) {
                            var nodes = [];
                            //$myhttp.get('/api/department/son/' + node.id).then(function (ns) {
                            $department.son(node.id).then(function (ns) {
                                nodes = ns;
                                _.each(ns, function (e) {
                                    e.checked = node.checked;
                                });
                                if (!$scope.pSeloneclass)
                                    //return $myhttp.get('/api/user', {where: where, page: page, limit: limit});
                                    return $user.findAllPage(where, page, limit);
                            }).then(function (users) {
                                var us = [];
                                if ($scope.pLimit) {
                                    //us = users.data;
                                    us = users.rows;
                                    $scope.pTotal = users.count;
                                    $scope.pTpage = users.total;
                                } else
                                    us = users;
                                if (us && us.length > 0) {
                                    _.each(us, function (e) {
                                        e.checked = node.checked;
                                        e.ancestors = [node.id];
                                        if ($scope.pShowusers)
                                            nodes.push(e);
                                    });
                                }
                                recursive($scope.pDepartments, node.id, nodes, us);
                            });
                        } else
                            $scope.pDepusers = node.users;
                    } else {//收
                        node.collapsed = false;
                    }
                }
            };
            var recursive = function (node, id, nodes, users) {
                var row = node;
                if (row.id == id) {
                    row.nodes = nodes;
                    row.users = users;
                    $scope.pDepusers = node.users;
                } else {
                    _.each(row.nodes || [], function (el) {
                        recursive(el, id, nodes, users);
                    });
                }
            };

            //点击复选框
            $scope.change = function (node) {
                if ($scope.pSeloneclass) {//只选择一个班级的情况
                    $scope.pSelectedp = {};
                    if (node.checked) {
                        $scope.pSelectedp = node;
                        $scope.recurChange($scope.pDepartments.nodes, false);
                        node.checked = true;
                    }
                } else {
                    //此节点的叶子节点跟着变化
                    $scope.recurChange(node.nodes, node.checked);
                    //撤销选中时，同时撤销此节点所有祖先节点的选中
                    if (!node.checked)
                        $scope.recurAncestorChange($scope.pDepartments.nodes, node.ancestors, node.password);
                }
            };
            //点击复选框 叶子递归变化
            $scope.recurChange = function (nodes, checked) {
                _.each(nodes || [], function (el) {
                    el.checked = checked;
                    if (!el.password)
                        $scope.recurChange(el.nodes, checked);
                });
            };
            //撤销选中 点击复选框 祖先递归变化
            $scope.recurAncestorChange = function (nodes, ancestors, password) {
                _.each(nodes || [], function (el) {
                    if (password) {
                        if (_.include(ancestors, el.id) && el.checked) {
                            el.checked = false;
                            $scope.recurAncestorChange($scope.pDepartments.nodes, el.ancestors);
                        } else
                            $scope.recurAncestorChange(el.nodes, ancestors, password);
                    } else if (_.include(ancestors, el.id) && el.checked) {
                        el.checked = false;
                        $scope.recurAncestorChange(el.nodes, ancestors);
                    }
                });
            };

        }
    };
});

app.directive('mydatepicker', function () {
    return {
        restrict: 'AE',
        link: function (scope, elm) {
            $(elm).datetimepicker({
                lang: "ch",
                timepicker: false,
                format: 'Y-m-d',
                closeOnDateSelect: true,
                mask: true
            });
        }
    }
});

/**
 * transVals用#分隔
 */
app.directive('transMoreMetadata', function () {
    return {
        restrict: 'AE',
        scope: {
            transVals: '@',
            transKey: '@',
        },
        template: "{{name}}",
        controller: function ($scope, $metadata) {
            if (!_.isUndefined($scope.transVals)) {
                if($scope.transVals.indexOf('#') == 0){
                    var values = ($scope.transVals.substring(1,$scope.transVals.length - 1)).split('#');
                    $metadata.findAll({key: $scope.transKey, value: {$in: values}},[],['context']).then(function(r){
                        return r;
                    }).each(function(e){
                        if($scope.name)
                            $scope.name += e.context + '、';
                        else
                            $scope.name = e.context + '、';
                    }).then(function(){
                        if($scope.name)
                            $scope.name = $scope.name.substring(0, $scope.name.length - 1);
                    }).catch(function () {
                        $scope.name = "";
                    });
                }
            }
        }
    };
});