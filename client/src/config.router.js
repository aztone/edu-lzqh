'use strict';

/**
* Config for the router
*/
angular.module('app')
.run(['$rootScope', '$state', '$stateParams', '$localStorage', function ($rootScope, $state, $stateParams, $localStorage) {    
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.$on('$stateChangeStart',
    function(event, toState, toParams, fromState, fromParams){
        $localStorage.history = toState.name;
    });
}])
.config(['$stateProvider', '$urlRouterProvider', 'JQ_CONFIG', function ($stateProvider, $urlRouterProvider, JQ_CONFIG) {
    $urlRouterProvider.otherwise('/access/signin');
    $stateProvider.state('app', {
        abstract: true,
        url: '/app',
        templateUrl: 'views/app.html',
        resolve: {
            auth: auth
        }
    })
    .state('app.dashboard', {
        url: '/dashboard',
        templateUrl: 'views/app_dashboard.html'
    })
    /** 管理 */
    .state('app.system', {
        abstract: true,
        url: '/system',
        template: '<div ui-view class="fade-in-up"></div>'
    })
    
    .state('app.system.metadata', {
        url: '/metadata',
        templateUrl: 'views/system/metadata.html'
    })
    .state('app.system.config', {
        url: '/config',
        templateUrl: 'views/system/config.html'
    })
    .state('app.system.department', {
        url: '/department',
        templateUrl: 'views/system/department.html'
    })
    .state('app.system.user', {
        url: '/user/:property',
        templateUrl: 'views/system/user.html'
    })
    .state('app.system.user_detail_list', {
        url: '/user_detail',
        templateUrl: 'views/system/user_detail.html'
    })
    /** 系统 */
    .state('app.global', {
        abstract: true,
        url: '/global',
        template: '<div ui-view class="fade-in-up"></div>'
    })
    .state('app.global.global_meta', {
        url: '/global_meta',
        templateUrl: 'views/global/global_meta.html'
    })
    .state('app.global.global_config', {
        url: '/global_config',
        templateUrl: 'views/global/global_config.html'
    })
    .state('app.global.level', {
        url: '/level',
        templateUrl: 'views/global/level.html'
    })
    .state('app.global.permission', {
        url: '/permission',
        templateUrl: 'views/global/permission.html'
    })
    .state('app.global.tenant', {
        url: '/tenant',
        templateUrl: 'views/global/tenant.html'
    })
    /** 日常 */
    .state('app.business', {
        abstract: true,
        url: '/business',
        template: '<div ui-view class="fade-in-up"></div>'
    })
    .state('app.business.customer', {
        url: '/customer',
        templateUrl: 'views/business/customer.html'
    })
    /** 示例 */
    .state('app.example', {
        abstract: true,
        url: '/example',
        template: '<div ui-view class="fade-in-up"></div>'
    })
    .state('app.example.upload', {
        url: '/upload',
        templateUrl: 'views/example/upload.html'
    })
    /** 个人信息 */
    .state('app.profile', {
        abstract: true,
        url: '/profile',
        template: '<div ui-view class="fade-in-up"></div>'
    })
    .state('app.profile.password', {
        url: '/profile/password',
        templateUrl: 'views/system/profile.html'
    })
    /** 认证 */
    .state('access', {
        abstract: true,
        url: '/access',
        template: '<div ui-view class="fade-in-up"></div>'
    })
    .state('access.signin', {
        url: '/signin',
        templateUrl: 'views/page_signin.html'
    })
    .state('access.signup', {
        url: '/signup',
        templateUrl: 'views/page_signup.html'
    })
    .state('access.forgotpwd', {
        url: '/forgotpwd',
        templateUrl: 'views/page_forgotpwd.html'
    })
    /** 图书管理 */
    .state('app.books', {
        abstract: true,
        url: '/books',
        template: '<div ui-view class="fade-in-up"></div>'
    })
    .state('app.books.list', {
        url: '/books/list',
        templateUrl: 'views/books/list.html'
    })
    /** 书单管理 */
    .state('app.booklist', {
        abstract: true,
        url: '/booklist',
        template: '<div ui-view class="fade-in-up"></div>'
    })
    .state('app.booklist.list', {
        url: '/booklist/list',
        templateUrl: 'views/booklist/list.html'
    })
    /** 语文阅读 */
    .state('app.chinese_reading', {
        abstract: true,
        url: '/chinese_reading',
        template: '<div ui-view class="fade-in-up"></div>'
    })
    .state('app.chinese_reading.book_list_made', {
        url: '/chinese_reading/book_list_made',
        templateUrl: 'views/chinese_reading/book_list_made.html'
    })
    .state('app.chinese_reading.book_list', {
        url: '/chinese_reading/book_list',
        templateUrl: 'views/chinese_reading/book_list.html'
    })
    .state('app.chinese_reading.book_shelves', {
        url: '/chinese_reading/book_shelves',
        templateUrl: 'views/chinese_reading/book_shelves.html'
    })
    /** 朗读测评 */
    .state('app.reading_assess', {
        abstract: true,
        url: '/reading_assess',
        template: '<div ui-view class="fade-in-up"></div>'
    })
    .state('app.reading_assess.words', {
        url: '/reading_assess/words',
        templateUrl: 'views/reading_assess/words.html'
    })
    .state('app.reading_assess.presentational', {
        url: '/reading_assess/presentational',
        templateUrl: 'views/reading_assess/presentational.html'
    })
    .state('app.reading_assess.essay', {
        url: '/reading_assess/essay',
        templateUrl: 'views/reading_assess/essay.html'
    })
    /** 测评中心 */
    .state('app.assess', {
        abstract: true,
        url: '/assess',
        template: '<div ui-view class="fade-in-up"></div>'
    })
    .state('app.assess.words', {
        url: '/assess/my_score',
        templateUrl: 'views/assess/my_score.html'
    })
    .state('app.assess.view_score', {
        url: '/assess/view_score',
        templateUrl: 'views/assess/view_score.html'
    });
    

    function auth(UserService, $state, $timeout) {
        
        //console.log(navigator.userAgent);
        function is_weixn(){
            var ua = navigator.userAgent.toLowerCase();
            if(ua.match(/micromessenger/i)) {
                // 是微信
            } else {
                // 不是微信
            }
        }

        var promise = new Promise(function(resolve, reject){
            UserService.getUser().then(function () {
                if (UserService.user) {
                    resolve();
                } else {
                    reject();
                    $timeout(function () {
                        $state.go('access.signin');
                    });
                }
            });
        });
        return promise;
    }
}]);
