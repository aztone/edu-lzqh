'use strict';
var app = angular.module('app');
app.factory('$socket', function ($rootScope, UserService, $timeout, $localStorage) {
    var service = {};
    var socket = io.connect();
    var ctiToken;
    socket.on('connect', function () {
        if ($localStorage.token) {
            service.register($localStorage.token);
        }
        if (ctiToken) {
            service.ctiRegister(ctiToken);
        }
    });

    socket.on('evt', function(msg){
        $rootScope.$broadcast('evt', msg);
    });

    socket.on('cti_evt', function(msg){
        $rootScope.$broadcast('cti_evt', msg);
    });

    service.register = function (t) {
        socket.emit('register', { token: t });
    };

    service.unregister = function (t) {
        socket.emit('unregister', { token: t });
    };

    service.ctiRegister = function (t) {
        ctiToken = t;
        socket.emit('cti_register', { token: t });
    };

    service.ctiUnRegister = function (t) {
        t = t || ctiToken;
        socket.emit('cti_unregister', { token: t });
    };

    socket.on('cti_evt', function(data){
        $rootScope.$broadcast('cti_evt', data);
        console.log('cti_evt', data);
    });

    return service;
});