/**
 * @fileOverview app常用服务
 * @author wing ying_gong@bjdv.com
 */
'use strict';
var app = angular.module('app');
var timeout = 15 * 60 * 1000;
app.factory('GlobalMetaService', function ($global_meta) {
    var service = {};
    var metas = {};
    service.trans = function (key, value) {
        var item;
        return Promise.resolve().then(function () {
            item = metas[key];
            var now = new Date().getTime();
            if (!item || now - item.time > timeout) {
                return $global_meta.findAll({ key: key, status: 1 }, [], ['context', 'value']).then(function (r) {
                    metas[key] = { rows: r, time: now };
                    item = metas[key];
                });
            }
        }).then(function () {
            var result = "";
            _.each(item.rows, function (row) {
                if (row.value == value) {
                    result = row.context;
                }
            });
            return result;
        });
    };
    return service;
});

app.factory('MetadataService', function ($metadata, $user) {
    var service = {};
    var metas = {};
    service.trans = function (key, value) {
        var item;
        return Promise.resolve().then(function () {
            item = metas[key];
            var now = new Date().getTime();
            if (!item || now - item.time > timeout) {
                return $metadata.findAll({ key: key, status: 1 }).then(function (r) {
                    metas[key] = { rows: r, time: now };
                    item = metas[key];
                });
            }
        }).then(function () {
            var result = "";
            _.each(item.rows, function (row) {
                if (row.value == value) {
                    result = row.context;
                }
            });
            return result;
        });
    };
    service.transUser = function (id) {
        return $user.findOne({ id: id }, ['name']).then(function (row) {
            return row.name;
        });
    };
    return service;
});

app.factory('UserService', function ($myhttp) {
    var service = {};

    service.getUser = function () {
        return $myhttp.get('/api/info').then(function (user) {
            service.user = user;
            return user;
        });
    };

    service.has = function (permission) {
        if (service.user && service.user.permissions) {
            var permissions = service.user.permissions;
            return _.includes(permissions, permission);
        }
        return false;
    };

    return service;
});