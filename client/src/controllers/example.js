var app = angular.module('app');
app.controller('ExampleCtrl', function($scope, $notify, $myhttp, Upload){
    $scope.importContent = function(){
        $myhttp.get('/api/import/' + $scope.importFile).then(function(r){
            $notify.info('内容', r);
        }).catch(function(e){
            $notify.warn('内容', e.message);
        });
    };

    $scope.showImage = function(){
        Upload.base64DataUrl([$scope.imageFile]).then(function(urls){
            $scope.imageUrl = urls[0];
        });
    };
});