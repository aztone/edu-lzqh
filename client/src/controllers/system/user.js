/**
 * @fileoverview 坐席操作
 * @author wing ying_gong@bjdv.com
 */
'use strict';
var app = angular.module('app');
app.controller('UserCtrl', function ($scope, $stateParams, $state, $notify, UserService, $uibModal, $department, $user) {
    var property = $stateParams.property;
    if (property)
        $scope.property = parseInt(property);
    $scope.myself = UserService.user;
    $scope.userDepartment;
    $scope.parentDepartment;
    $scope.departments = {};
    $scope.depusers = [];

    var mydeps = ($scope.myself.department_id.substring(1, $scope.myself.department_id.length - 1)).split('#');
    var mydep_ids = [0];
    _.forEach(mydeps, function (e) {
        mydep_ids.push(parseInt(e));
    });
    $department.findAll({ id: { $in: mydep_ids } }).then(function (department) {
        $scope.userDepartment = department;
        return $department.son(null);
    }).then(function (d) {
        $scope.departments.nodes = d;
    });

    $scope.cond = {};
    $scope.page = { page: 1, limit: 10 };
    $scope.search = function (page) {
        page = page || $scope.page.page;
        var where = {
            property: property || undefined,
            status: 1,
            department_id: { $like: '%#' + $scope.parentDepartment.id + '#%' }
        };
        /**
        if(!$scope.cond.like){
            where = {};
        }else{
            where ={ $or: [ { name: { $like: '%' + $scope.cond.like + '%' } }] };
        }
         */
        $user.findAllPage(where, page, $scope.page.limit, ['id']).then(function (r) {
            $scope.depusers = r.rows;
            $scope.page.page = r.page;
            $scope.page.limit = r.limit;
            $scope.page.count = r.count;
            $scope.page.total = r.total;
        });
    };

    //$scope.search();

    $scope.add = function () {
        var parentDepartment = $scope.parentDepartment;
        $uibModal.open({
            size: 'md',
            controller: 'UserAddCtrl',
            templateUrl: 'views/system/user_add.html',
            resolve: {
                department: function () {
                    return parentDepartment;
                },
                property: function () {
                    return $scope.property;
                }
            }
        }).result.then(function () {
            $scope.search(1);
        }, function () {
            $scope.search(1);
        });
    };

    $scope.edit = function (user) {
        $uibModal.open({
            size: 'md',
            controller: 'UserEditCtrl',
            templateUrl: 'views/system/user_edit.html',
            resolve: {
                user: function () {
                    return user;
                }
            }
        }).result.then(function () {
            $scope.search(1);
        }, function () {
            $scope.search(1);
        });
    };

    //给教师分配班级课程
    $scope.assignSubject = function (user) {
        $uibModal.open({
            size: 'md',
            templateUrl: 'views/system/assign_subject.html',
            controller: 'AssignSubjectCtrl',
            resolve: {
                user: function () {
                    return user;
                }
            }
        }).result.then(function () {
            $scope.search(1);
        });
    };

    $scope.remove = function (user) {
        if (user.property == 4 && confirm('确定删除学生？')) {
            $user.remove(user.id).then(function () {
                $notify.info('删除学生', '成功');
                $scope.search(1);
            }).catch(function (e) {
                $notify.warning('删除学生', e.message);
                $scope.search(1);
            });
        } else {
            var parentDepartment = $scope.parentDepartment;
            $uibModal.open({
                size: 'sz',
                templateUrl: 'remove_sel.html',
                controller: 'UserRemoveCtrl',
                resolve: {
                    user: function () {
                        return user;
                    },
                    parentDepartment: function () {
                        return parentDepartment;
                    }
                }
            }).result.then(function () {
                $scope.search(1);
            }, function () {
            });
        }
    };
});

/**教师删除 */
app.controller('UserRemoveCtrl', function ($scope, $state, $notify, $uibModalInstance, UserService,
    $myhttp, $user, $teachersubject, $level, user, parentDepartment) {
    $scope.user = user;
    $scope.user.remove_sel = 1;
    $scope.save = function () {
        if ($scope.user.remove_sel == 2) {//从系统中删除
            $user.remove(user.id).then(function () {
                return $teachersubject.findAll({ t_id: user.id }, [], ['id']);
            }).each(function (eml) {
                return $teachersubject.remove(eml.id);
            }).then(function () {
                $notify.info('从系统中删除教师', '成功');
                $uibModalInstance.close();
            }).catch(function (e) {
                $notify.warning('从系统中删除教师', e.message);
                $uibModalInstance.close();
            });
        } else {
            var user_deps = (user.department_id.substring(1, user.department_id.length - 1)).split('#');
            if (user_deps.length > 1)
                $scope.user.department_id = user.department_id.replaceAll('#' + parentDepartment.id + '#', '#');
            else
                $scope.user.department_id = '#0#';
            $user.save($scope.user.id, $scope.user).then(function () {
                return $teachersubject.findAll({ t_id: user.id, department: parentDepartment.id }, [], ['id']);
            }).each(function (eml) {
                return $teachersubject.remove(eml.id);
            }).then(function () {
                $notify.info('从本部门删除教师', '成功');
                $uibModalInstance.close();
            }).catch(function (e) {
                $notify.warning('从本部门删除教师', e.message);
                $uibModalInstance.close();
            });
        }
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

app.controller('UserAddCtrl', function ($scope, $state, $notify, $uibModalInstance, $user, $level, UserService, property, department) {
    $scope.user = { property: property, status: 1 };
    $scope.passwd = {};
    $level.findAll({}, [], ['id', 'level', 'name']).then(function (rows) {
        $scope.levels = rows;
    });
    $scope.save = function () {
        $scope.user.password = $.md5($scope.passwd.password);
        $scope.user.department_id = "#" + department.id + '#';
        $scope.user.create_by = UserService.user.id;
        $user.create($scope.user).then(function () {
            $notify.info('添加成员', '成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('添加成员', e.message);
            $uibModalInstance.close();
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

app.controller('UserEditCtrl', function ($scope, $state, $notify, $uibModalInstance, user, $user, $level) {
    $scope.user = user;
    $scope.passwd = {};
    $level.findAll({}, [], ['id', 'level', 'name']).then(function (rows) {
        $scope.levels = rows;
    });

    $scope.save = function () {
        if ($scope.passwd.password) {
            $user.password = $.md5($scope.passwd.password);
        }
        $user.save($scope.user.id, $scope.user).then(function () {
            $notify.info('修改成员', '成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('修改成员', e.message);
            $uibModalInstance.close();
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

/**
 * 给老师分配班级课程
 */
app.controller('AssignSubjectCtrl', function ($scope, $state, $notify, $uibModalInstance, user, $user, $teachersubject, $metadata, $department, UserService) {
    $scope.user = user;
    $scope.departmentsGroup = [];
    $scope.selDepartment;
    $scope.teacherrole = {};
    $scope.trList = [];
    $scope.originalTrlist = [];

    $teachersubject.findAll({ t_id: $scope.user.id }).then(function (r) {
        $scope.trList = r;
        $scope.originalTrlist = r;
        return $metadata.findAll({ key: 'subject', status: 1 });
    }).then(function (result) {
        $scope.subjectList = result;
        return $department.son(null);
    }).then(function (r) {
        $scope.dep_root = r[0];
        pickSon(r[0].id);
    });

    var pickSon = function (parent) {
        $department.son(parent).then(function (result) {
            if (result.length > 0) {
                $scope.departmentsGroup.push({ departments: result });
            }
        }, function (err) {
            $notify.warning('获取子部门user.js256', err.message);
        });
    };
    
    $scope.departmentChange = function (index) {
        $scope.index = index;
        if ($scope.departmentsGroup[index].selected) {
            $scope.departmentsGroup = $scope.departmentsGroup.splice(0, index + 1);
            $scope.selDepartment = $scope.departmentsGroup[index].selected;
        } else {
            if (index > 0) {
                $scope.departmentsGroup = $scope.departmentsGroup.splice(0, index);
                $scope.selDepartment = $scope.departmentsGroup[index - 1].selected;
            } else {
                $scope.departmentsGroup = [];
                pickSon($scope.dep_root.id);
                $scope.selDepartment = undefined;//$scope.department;
            }
        }
        if ($scope.selDepartment)
            pickSon($scope.selDepartment.id);
    };

    $scope.showCt = true;//班主任复选框是否显示
    $scope.$watch('selDepartment', function () {
        $scope.teacherrole.checked = false;
        if ($scope.selDepartment && $scope.selDepartment.property == 3) {//3表示选择的部门是确定班级
            $scope.trList = $scope.originalTrlist;
            $teachersubject.findAll({ department: $scope.selDepartment.id }).then(function (r) {
                if (r && r.length > 0)
                    $scope.depTeachers = _.reject($scope.subjectList, function (e) {
                        return _.result(_.find(r, { 'subject': e.value }), 'subject') == e.value;
                    });
                else
                    $scope.depTeachers = $scope.subjectList;
                var d = _.find(r, { 'role': 'B' });
                if (d)
                    $scope.showCt = false;
                else
                    $scope.showCt = true;
            });
        }
    });

    $scope.addSubject = function () {
        $scope.teacherrole.role = 'A';//老师
        if ($scope.teacherrole.checked)
            $scope.teacherrole.role = 'B';//班主任
        //选择部门是确定班级
        if ($scope.selDepartment.property != 3 || !$scope.teacherrole.role || !$scope.teacherrole.subject)
            alert('请精确到班级再指定教师科目角色！');
        if (_.result(_.find($scope.trList, { 'department': $scope.selDepartment.id, 'subject': $scope.teacherrole.subject }), 't_id'))
            alert('已存在,请勿重复添加！');
        else {
            $scope.trList.push({
                department: $scope.selDepartment.id,
                t_id: $scope.user.id,
                role: $scope.teacherrole.role,
                subject: $scope.teacherrole.subject
            });
            if ($scope.trList && $scope.trList.length > 0)
                $scope.depTeachers = _.reject($scope.depTeachers, function (e) {
                    return _.result(_.find($scope.trList, { 'subject': e.value }), 'subject') == e.value;
                });
            if ($scope.teacherrole.role == 'B')
                $scope.showCt = false;
            else
                $scope.showCt = true;
        }
    };
    $scope.removeSubject = function (trl) {
        var i = 0;
        _.forEach($scope.trList, function (eml) {
            if (eml && trl.department == eml.department && trl.subject == eml.subject) {
                $scope.trList.splice(i, 1);
                $scope.depTeachers.push(_.find($scope.subjectList, { 'value': eml.subject }));
                if (trl.role == 'A')
                    $scope.showCt = false;
                else
                    $scope.showCt = true;
            }
            i++;
        });
    };

    $scope.save = function () {
        if (!$scope.trList || $scope.trList.length <= 0)
            alert('请确保至少存在一条任课信息！');
        else {
            var deps = '';
            $teachersubject.findAll({ t_id: $scope.user.id }, [], ['id']).each(function (tr) {
                return $teachersubject.remove(tr.id);
            }).then(function () {
                deps = $scope.user.department_id;
                return $scope.trList;
            }).each(function (eml) {
                if (deps.indexOf("#" + eml.department + "#") < 0) {
                    if (deps && deps != '' && deps != '#0#')
                        deps += eml.department + "#";
                    else
                        deps = "#" + eml.department + "#";
                }
                eml.create_by = UserService.user.id;
                return $teachersubject.create(eml);
            }).then(function () {
                $scope.user.department_id = deps;
                return $user.save($scope.user.id, $scope.user);
            }).then(function () {
                $notify.info('保存任课信息', '成功');
                $uibModalInstance.close();
            }).catch(function (e) {
                $notify.warning('保存任课信息失败', e.message);
                $uibModalInstance.close();
            });
        }
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

});

app.controller('ProfileCtrl', function ($scope, $state, $notify, $user, UserService, $localStorage) {

    UserService.getUser().then(function (user) {
        $scope.user = user;
        return $user.findOne(user.id);
    }).then(function (row) {
        $scope.oriPassword = row.password;
    });


    $scope.save = function () {
        if ($scope.oriPassword != $.md5($scope.user.password)) {
            $notify.warning('修改密码', '原始密码错误');
            return;
        }
        if ($scope.user.new_password != $scope.user.new_password2) {
            $notify.warning('修改密码', '两次密码输入不一致');
            return;
        } else {
            $user.password = $.md5($scope.user.new_password);
        }
        $user.save($scope.user.id, $scope.user).then(function () {
            $notify.info('修改密码', '成功');
            $scope.logout();
        }).catch(function (e) {
            $notify.warning('修改密码', e.message);
        });
    };

    $scope.logout = function () {
        delete $localStorage.token;
        delete $localStorage.user;
        $state.go('access.signin');
    };
});

app.controller('UserDetailCtrl', function ($scope, $state, $notify, $uibModal, $user_detail) {
    $scope.page = { limit: 10 };
    $scope.search = function (page) {
        page = page || $scope.page.page;
        var where;
        if ($scope.info == undefined || $scope.info == '') {
            where = {};
        } else {
            where = { $or: [{ alis: { $like: '%' + $scope.info + '%' } }, { name: { $like: '%' + $scope.info + '%' } }] };
        }
        $user_detail.findAllPage(where, page, $scope.page.limit, ['id']).then(function (r) {
            $scope.users = r.rows;
            $scope.page.page = r.page;
            $scope.page.limit = r.limit;
            $scope.page.count = r.count;
            $scope.page.total = r.total;
        });
    };

    $scope.search();


});
