/**
 * @fileOverview 部门操作
 * @author wing ying_gong@bjdv.com
 */
'use strict';
var app = angular.module('app');

app.controller('DepartmentCtrl', function ($scope, $notify, $uibModal, $department) {

    $scope.data = {};

    var recursive = function (node, id, nodes) {
        var row = node;
        if (row.id == id) {
            row.nodes = nodes;
        } else {
            _.each(row.nodes || [], function (el) {
                recursive(el, id, nodes);
            });
        }
    };

    $scope.expand = function (id) {
        id = id || null;
        $department.son(id).then(function (rows) {
            _.each(rows, function (d) {
                d.spaces = "｜－－";
                _.times(d.depth, function () {
                    d.spaces += "｜－－";
                });
            });
            if (!id) {
                $scope.data.nodes = rows;
            } else {
                recursive($scope.data, id, rows);
            }
            if (rows.length == 0) {
                $notify.info('没有子部门');
            }
        });
    };

    $scope.expand();

    $scope.init = function () {
        $uibModal.open({
            size: 'md',
            templateUrl: 'department_add.html',
            controller: 'DepartmentAddCtrl',
            resolve: {
                node: function () {
                    return {};
                }
            }
        }).result.then(function () {
            $scope.expand();
        }, function () {
        });
    };

    $scope.add = function (node) {
        $uibModal.open({
            size: 'md',
            templateUrl: 'department_add.html',
            controller: 'DepartmentAddCtrl',
            resolve: {
                node: function () {
                    return node;
                }
            }
        }).result.then(function () {
            $scope.expand(node.id);
        }, function () {
        });
    };

    $scope.edit = function (node) {
        $uibModal.open({
            size: 'md',
            templateUrl: 'department_edit.html',
            controller: 'DepartmentEditCtrl',
            resolve: {
                node: function () {
                    return node;
                }
            }
        }).result.then(function () {
            $scope.expand(node.parent);
        }, function () {
        });
    };
    /**
    $scope.member = function (node) {
        $uibModal.open({
            size: 'sm',
            templateUrl: 'department_member.html',
            controller: 'DepartmentMemberCtrl',
            resolve: {
                node: function () {
                    return node;
                }
            }
        }).result.then(function () {
        }, function () {
        });
    };
     */

    $scope.remove = function (node) {
        return $department.remove(node.id).then(function () {
            $notify.success('删除节点', '成功');
            $scope.expand(node.parent);
        }).catch(function (e) {
            $notify.warning('删除节点', e.message);
        });
    };

});

app.controller('DepartmentAddCtrl', function ($scope, $notify, $uibModalInstance, $department, node) {
    var a = [{key: 1, value: '学段'},{key: 2, value: '年级'},{key: 3, value: '班级'},{key: 4, value: '其他'},];
    $scope.department = {};
    if(node)
        $scope.dpropertys = _.remove(a,function(e){return e.key>node.property;});
    else
        $scope.dpropertys = a;

    $scope.save = function () {
        var parent = node ? node.id : undefined;
        $scope.department.status = 1;
        $department.create($scope.department, parent).then(function () {
            $notify.success('添加节点', "成功");
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('添加节点', e.message);
            $uibModalInstance.close();
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

app.controller('DepartmentEditCtrl', function ($scope, $notify, $uibModalInstance, $department, node) {
    $scope.department = node.name;
    $scope.save = function () {
        $department.save($scope.department.id, $scope.department).then(function () {
            $notify.success('修改节点', "成功");
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('修改节点', e.message);
            $uibModalInstance.close();
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

app.controller('DepartmentMemberCtrl', function ($scope, $notify, $timeout, $uibModalInstance, $department, $user, node) {
    $scope.department = node;
    $scope.selected = [];

    $user.findAll({status: 1}, ['id'], ['id', 'name', 'department_id']).then(function(rows){
        $scope.users = rows;
        $scope.selected = _.map(_.filter(rows, {department_id: $scope.department.id}), 'id');
    });

    $scope.save = function(){
        Promise.resolve().then(function(){
            return $scope.selected;
        }).each(function(id){
            return $user.save(id, {department_id: $scope.department.id});
        }).then(function(){
            $notify.success('部门成员', '修改成功');
            $uibModalInstance.close();
        }).catch(function(e){
            $notify.warning(e.message);
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});