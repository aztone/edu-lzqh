var app = angular.module('app');

/**
 * 图书管理
 */
app.controller('BookManageCtrl', function ($scope, $state, $notify, $uibModal, $metadata, $books) {
    $metadata.findAll({ status: 1, key: { $in: ['book:type', 'press'] } }, [], ['key', 'value', 'context']).then(function (rows) {
        $scope.bookTypeList = _.filter(rows, { key: 'book:type' });
        $scope.pressList = _.filter(rows, { key: 'press' });
    });

    $scope.cond = { book_types: [] };
    $scope.page = { page: 1, limit: 10 };

    $scope.search = function (page) {
        page = page || $scope.page.page;
        var where = {
            status: 1
        };

        if ($scope.cond.like) {
            /**模糊查询：书名、作者、名家推荐语、图书描述、图书简介、出版社 */
            where.$or = [
                { name: { $like: '%' + $scope.cond.like + '%' } },
                { author: { $like: '%' + $scope.cond.like + '%' } },
                { recommended_language: { $like: '%' + $scope.cond.like + '%' } },
                { description: { $like: '%' + $scope.cond.like + '%' } },
                { introduction: { $like: '%' + $scope.cond.like + '%' } },
                { press: { $like: '%' + $scope.cond.like + '%' } }];
        }
        Promise.resolve().then(function () {
            return $scope.cond.book_types;
        }).each(function (e) {
            if (e)
                if (where.$or)
                    where.$or.push({ book_type: { $like: '%#' + e + '#%' } });
                else
                    where.$or = [{ book_type: { $like: '%#' + e + '#%' } }];
        }).then(function () {
            return $books.findAllPage(where, page, $scope.page.limit, ['id']).then(function (r) {
                $scope.books = r.rows;
                $scope.page.page = r.page;
                $scope.page.limit = r.limit;
                $scope.page.count = r.count;
                $scope.page.total = r.total;
            });
        });

    };
    $scope.search(1);

    $scope.add = function () {
        $uibModal.open({
            size: 'lg',
            controller: 'BooksAddCtrl',
            templateUrl: 'views/books/add.html'
        }).result.then(function () {
            $scope.search(1);
        }, function () {
            $scope.search(1);
        });
    };

    $scope.edit = function (book) {
        if (book.book_type && book.book_type.length > 2)
            book.book_types = (book.book_type.substring(1, book.book_type.length - 1)).split('#');

        $uibModal.open({
            size: 'lg',
            controller: 'BooksEditCtrl',
            templateUrl: 'views/books/edit.html',
            resolve: {
                book: function () {
                    return book;
                }
            }
        }).result.then(function () {
            $scope.search(1);
        }, function () {
            $scope.search(1);
        });
    };

});

app.controller('BooksAddCtrl', function ($scope, $state, $notify, $uibModalInstance, $metadata, UserService, $books) {
    $metadata.findAll({ status: 1, key: { $in: ['book:type', 'press'] } }, [], ['key', 'value', 'context']).then(function (rows) {
        $scope.bookTypeList = _.filter(rows, { key: 'book:type' });
        $scope.pressList = _.filter(rows, { key: 'press' });
        console.log('bookTypeList', $scope.bookTypeList);
        console.log('pressList', $scope.pressList);
    });
    $scope.book = { status: 1, create_by: UserService.user.id };

    $scope.save = function () {
        if ($scope.book.book_types && $scope.book.book_types.length > 0)
            $scope.book.book_type = "#" + _.join($scope.book.book_types, '#') + "#";
        $books.create($scope.book).then(function () {
            $notify.info('添加图书', '成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('添加图书', e.message);
            $uibModalInstance.close();
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

});

app.controller('BooksEditCtrl', function ($scope, $state, $notify, $uibModalInstance, $metadata, UserService, $books, book) {
    $scope.book = book;
    $metadata.findAll({ status: 1, key: { $in: ['book:type', 'press'] } }, [], ['key', 'value', 'context']).then(function (rows) {
        $scope.bookTypeList = _.filter(rows, { key: 'book:type' });
        $scope.pressList = _.filter(rows, { key: 'press' });
        console.log('bookTypeList', $scope.bookTypeList);
        console.log('pressList', $scope.pressList);
    });

    $scope.save = function () {
        if ($scope.book.book_types && $scope.book.book_types.length > 0)
            $scope.book.book_type = "#" + _.join($scope.book.book_types, '#') + "#";
        $books.save($scope.book.id, $scope.book).then(function () {
            $notify.info('修改图书', '成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('修改图书', e.message);
            $uibModalInstance.close();
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };

});