/**
 * @fileoverview 客户操作
 * @author wing ying_gong@bjdv.com
 */
'use strict';
var app = angular.module('app');
app.controller('CustomerCtrl', function ($scope, $state, $notify, $uibModal, $customer) {
    $scope.cond = {};
    $scope.page = { limit: 10 };
    $scope.search = function (page) {
        page = page || $scope.page.page;
        var where;
        if(!$scope.cond.like){
            where = {};
        }else{
            where ={ $or: [{ code: { $like: '%' + $scope.cond.like + '%' } }, { name: { $like: '%' + $scope.cond.like + '%' } }] };
        }
        $customer.findAllPage(where, page, $scope.page.limit, ['id']).then(function (r) {
            $scope.customers = r.rows;
            $scope.page.page = r.page;
            $scope.page.limit = r.limit;
            $scope.page.count = r.count;
            $scope.page.total = r.total;
        });
    };

    $scope.search();

    $scope.add = function () {
        $uibModal.open({
            size: 'lg',
            controller: 'CustomerAddCtrl',
            templateUrl: 'views/business/customer_add.html',
        }).result.then(function () {
            $scope.search();
        }, function () {
            $scope.search();
        });
    };

    $scope.edit = function (customer) {
        $uibModal.open({
            size: 'lg',
            controller: 'CustomerEditCtrl',
            templateUrl: 'views/business/customer_edit.html',
            resolve: {
                customer: function () {
                    return customer;
                }
            }
        }).result.then(function () {
            $scope.search();
        }, function () {
            $scope.search();
        });
    };

    $scope.remove = function (customer) {
        $customer.remove(customer.id).then(function () {
            $notify.info('客户', '删除成功');
            $scope.search();
        }).catch(function (e) {
            $notify.warning('客户', e.message);
            $scope.search();
        });
    };
});

app.controller('CustomerAddCtrl', function ($scope, $state, $notify, $uibModalInstance, $customer) {
    $scope.customer = {};
    $scope.save = function () {
        $customer.create($scope.customer).then(function () {
            $notify.info('修改客户', '成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('修改客户', e.message);
            $uibModalInstance.close();
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

app.controller('CustomerEditCtrl', function ($scope, $state, $notify, $uibModalInstance, customer, $customer) {
    $scope.customer = customer;
    
    $scope.save = function () {
        $customer.save($scope.customer.id, $scope.customer).then(function () {
            $notify.info('修改客户', '成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('修改客户', e.message);
            $uibModalInstance.close();
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});


