/**
 * @fileOverview 权限操作
 * @author wing ying_gong@bjdv.com
 */
'use strict';
var app = angular.module('app');
app.controller('LevelCtrl', function ($scope, $state, $notify, $uibModal, $level,$permission,$level_permission) {
    $scope.page = { limit: 10 };
    $scope.cond = {};   

    $scope.add = function () {
        $uibModal.open({
            size: 'sm',
            templateUrl: 'level_add.html',
            controller: 'LevelAddCtrl'
        }).result.then(function () {
            $scope.search();
        }, function () {
            $scope.search();
        });
    };
    $scope.edit = function (item) {
        $level_permission.findAll({level_id: item.id}, [], ['permission_id']).then(function(rows){
            item.permissions = _.map(rows, 'permission_id');
        }).then(function(){
            $uibModal.open({
                size: 'sm',
                templateUrl: 'level_edit.html',
                controller: 'LevelEditCtrl',
                resolve: {
                    level: function () {
                        return item;
                    }
                }
            }).result.then(function () {
                $scope.search();
            }, function () {
                $scope.search();
            });
        });
    };

    $scope.remove = function (item) {
        $level.remove(item.id).then(function () {
            $scope.search();
        }).catch(function (e) {
            $notify.warning('等级权限', e.message);
        });
    };

    $scope.search = function (page) {
        var where = {};
        if ($scope.cond.searchLike) {
            where.name = { $like: '%' + _.trim($scope.cond.searchLike) + '%' };
        }
        $level.findAllPage(where, page, $scope.page.limit, [['level','DESC']]).then(function (r) {
            $scope.levels = r.rows;
            $scope.page.total = r.total;
            $scope.page.page = r.page;
            $scope.page.limit = r.limit;
            $scope.page.count = r.count;
        });
    };

    $scope.search();
});

app.controller('LevelAddCtrl', function ($scope, $state, $notify, $uibModalInstance, $level, $permission) {
    $scope.level = {};
    $permission.findAll({},[], ['id', 'name']).then(function(rows){
        $scope.permissions = rows;
    });
    $scope.save = function () {
        $level.create($scope.level).then(function () {
            $notify.info('等级权限', '保存成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('等级权限', e.message);
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

app.controller('LevelEditCtrl', function ($scope, $state, $notify, $uibModalInstance, level, $level, $permission, $level_permission) {
    $scope.level = level;
    $permission.findAll({},[], ['id', 'name']).then(function(rows){
        $scope.permissions = rows;
    //     return $level_permission.findAll({level_id: level.id}, [], ['permission_id']);
    // }).then(function(rows){
    //     $scope.level.permissions = _.map(rows, 'permission_id');
    });
    $scope.save = function () {
        $level.save($scope.level.id, $scope.level).then(function () {
            $notify.info('等级权限', '保存成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('等级权限', e.message);
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

app.controller('PermissionCtrl', function ($scope, $state, $notify, $uibModal, $global_meta, $permission) {
    $scope.page = { limit: 10 };
    $scope.cond = {};

    $global_meta.findAll({key: 'scope', status: 1}, [], ['context', 'value']).then(function(r){
        $scope.scopes = r;
    });   

    $scope.add = function () {
        $uibModal.open({
            size: 'sm',
            templateUrl: 'permission_add.html',
            controller: 'PermissionAddCtrl',
            resolve: {
                scopes: function(){
                    return $scope.scopes;
                }
            }
        }).result.then(function () {
            $scope.search();
        }, function () {
            $scope.search();
        });
    };
    $scope.edit = function (item) {
        $uibModal.open({
            size: 'sm',
            templateUrl: 'permission_edit.html',
            controller: 'PermissionEditCtrl',
            resolve: {
                permission: function () {
                    return item;
                },
                scopes: function(){
                    return $scope.scopes;
                }
            }
        }).result.then(function () {
            $scope.search();
        }, function () {
            $scope.search();
        });
    };

    $scope.remove = function (item) {
        $permission.remove(item.id).then(function () {
            $notify.info('功能权限', '删除成功');
            $scope.search();
        }).catch(function (e) {
            $notify.warning('功能权限', e.message);
        });
    };

    $scope.search = function (page) {
        var where = {};
        if ($scope.cond.searchLike) {
            where.name = { $like: '%' + _.trim($scope.cond.searchLike) + '%' };
        }
        $permission.findAllPage(where, page, $scope.page.limit).then(function (r) {
            $scope.permissions = r.rows;
            $scope.page.total = r.total;
            $scope.page.page = r.page;
            $scope.page.limit = r.limit;
            $scope.page.count = r.count;
        });
    };

    $scope.search();
});

app.controller('PermissionAddCtrl', function ($scope, $state, $notify, $uibModalInstance, scopes, $permission) {
    $scope.permission = {};
    $scope.scopes = scopes;
    $scope.save = function () {
        $permission.create($scope.permission).then(function () {
            $notify.info('功能权限', '保存成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('功能权限', e.message);
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});

app.controller('PermissionEditCtrl', function ($scope, $state, $notify, $uibModalInstance, permission, scopes, $permission) {
    $scope.permission = permission;
    $scope.scopes = scopes;
    $scope.save = function () {
        $permission.save($scope.permission.id, $scope.permission).then(function () {
            $notify.info('功能权限', '保存成功');
            $uibModalInstance.close();
        }).catch(function (e) {
            $notify.warning('功能权限', e.message);
        });
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };
});