/**
 * @fileOverview 认证操作
 * @author wing ying_gong@bjdv.com
 */
'use strict';
var app = angular.module('app');
app.controller('SigninCtrl', function ($scope, $myhttp, $state, $notify, $localStorage,$socket) {
    $scope.user = {};
    $scope.remember = $localStorage.remember;

    if ($scope.remember) {
        $scope.uname = $localStorage.uname;
        $scope.passwd = $localStorage.passwd;
    }

    $scope.login = function () {
        $scope.user.password = $.md5($scope.passwd);
        //$myhttp.post('/access/auth', {
        $myhttp.post('/access/login', {
            user_name: $scope.uname,
            password: $scope.user.password
        }).then(function (r) {
            $localStorage.remember = $scope.remember;
            $localStorage.uname = $scope.uname;
            $localStorage.passwd = $scope.passwd;

            $localStorage.token = r.token;
            $localStorage.user = r.user;
            $socket.register($localStorage.token);
        }).then(function () {            
            $state.go('app.dashboard');
        }).catch(function (e) {
            $notify.warning("登录", e.message);
        });
    };
});

app.controller('SignupCtrl', function ($scope, $state, $notify, $myhttp) {
    $scope.user = {status:1, property: 1};

    //系统中学校列表
    $myhttp.get('/access/tenants',{where:{status:1}}).then(function(ts){
        $scope.tenants = ts;
    });

    $scope.$watch('user.passwd',function(){
        if($scope.user.passwd!=$scope.user.passwd2)
            $scope.passwd_mes = '两次密码输入不一致';
        else
            $scope.passwd_mes = undefined;
    });
    $scope.$watch('user.passwd2',function(){
        if($scope.user.passwd!=$scope.user.passwd2)
            $scope.passwd_mes = '两次密码输入不一致';
        else
            $scope.passwd_mes = undefined;
    });

    $scope.signup = function () {
        if(!$scope.passwd_mes){//手机收到的验证码判断一下
            $scope.user.name = $scope.user.user_name;
            $scope.user.department_id = 0;
            $scope.user.opt_by = 0;

            $scope.user.password = $.md5($scope.user.passwd);
            $myhttp.post('/access/signup', {user: $scope.user}).then(function () {
                $notify.info('注册', '注册成功');
                $state.go('access.signin');
            }).catch(function (e) {
                $notify.warning('注册', e.message);
            });
        }
    };
});

app.controller('ForgotpwdCtrl', function ($scope, $state, $notify, $myhttp) {
    $scope.user = {};

    $scope.$watch('user.passwd',function(){
        if($scope.user.passwd!=$scope.user.passwd2)
            $scope.passwd_mes = '两次密码输入不一致';
        else
            $scope.passwd_mes = undefined;
    });
    $scope.$watch('user.passwd2',function(){
        if($scope.user.passwd!=$scope.user.passwd2)
            $scope.passwd_mes = '两次密码输入不一致';
        else
            $scope.passwd_mes = undefined;
    });

    $scope.getValidCode = function(){
        $scope.authError = undefined;
        if($scope.user.tel){
            $myhttp.get('/access/bytel',{tel: $scope.user.tel}).then(function(user){
                $scope.orgUser = user;
                //调短信接口发送验证码
            }).catch(function (e) {
                $scope.authError = '电话号码未注册';
                $notify.warning("电话号码有误", e.message);
            });
        }else
            alert('请输入手机号码');
    };

    $scope.findpwd = function () {
        if(!$scope.passwd_mes && $scope.orgUser && $scope.orgUser.id){//手机收到的验证码判断一下
            $scope.orgUser.password = $.md5($scope.user.passwd);
            $myhttp.put('/access/'+$scope.orgUser.id, {user: $scope.orgUser}).then(function () {
                $notify.info('密码找回', '密码修改成功');
                $state.go('access.signin');
            }).catch(function (e) {
                $notify.warning('密码找回', e.message);
            });
        }
    };
});

app.controller('LogoutCtrl', function ($scope, $state, $myhttp, $localStorage, $socket) {
    $scope.logout = function () {
        $socket.unregister($localStorage.token);
        $socket.ctiUnRegister();
        delete $localStorage.token;
        delete $localStorage.user;
        $state.go('access.signin');
    };
});

app.controller('UserInfoCtrl', function($scope, UserService){
    $scope.user = UserService.user;
});