// lazyload config

angular.module('app')
    /**
   * jQuery plugin config use ui-jq directive , config the js and css files that required
   * key: function name of the jQuery plugin
   * value: array of the css js file located
   */
    .constant('JQ_CONFIG', {
        easyPieChart: ['../node_modules/weblibs/jquery.easy-pie-chart/dist/jquery.easypiechart.fill.js'],
        sparkline: ['../node_modules/weblibs/jquery.sparkline/dist/jquery.sparkline.retina.js'],
        plot: ['../node_modules/weblibs/flot/jquery.flot.js',
            '../node_modules/weblibs/flot/jquery.flot.pie.js',
            '../node_modules/weblibs/flot/jquery.flot.resize.js',
            '../node_modules/weblibs/flot.tooltip/js/jquery.flot.tooltip.js',
            '../node_modules/weblibs/flot.orderbars/js/jquery.flot.orderBars.js',
            '../node_modules/weblibs/flot-spline/js/jquery.flot.spline.js'],
        moment: ['../node_modules/weblibs/moment/moment.js'],
        screenfull: ['../node_modules/weblibs/screenfull/dist/screenfull.min.js'],
        slimScroll: ['../node_modules/weblibs/slimscroll/jquery.slimscroll.min.js'],
        sortable: ['../node_modules/weblibs/html5sortable/jquery.sortable.js'],
        nestable: ['../node_modules/weblibs/nestable/jquery.nestable.js',
            '../node_modules/weblibs/nestable/jquery.nestable.css'],
        filestyle: ['../node_modules/weblibs/bootstrap-filestyle/src/bootstrap-filestyle.js'],
        slider: ['../node_modules/weblibs/bootstrap-slider/bootstrap-slider.js',
            '../node_modules/weblibs/bootstrap-slider/bootstrap-slider.css'],        
        TouchSpin: ['../node_modules/weblibs/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js',
            '../node_modules/weblibs/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css'],
        wysiwyg: ['../node_modules/weblibs/bootstrap-wysiwyg/bootstrap-wysiwyg.js',
            '../node_modules/weblibs/bootstrap-wysiwyg/external/jquery.hotkeys.js'],
        dataTable: ['../node_modules/weblibs/datatables/media/js/jquery.dataTables.min.js',
            '../node_modules/weblibs/plugins/integration/bootstrap/3/dataTables.bootstrap.js',
            '../node_modules/weblibs/plugins/integration/bootstrap/3/dataTables.bootstrap.css'],
        vectorMap: ['../node_modules/weblibs/bower-jvectormap/jquery-jvectormap-1.2.2.min.js',
            '../node_modules/weblibs/bower-jvectormap/jquery-jvectormap-world-mill-en.js',
            '../node_modules/weblibs/bower-jvectormap/jquery-jvectormap-us-aea-en.js',
            '../node_modules/weblibs/bower-jvectormap/jquery-jvectormap-1.2.2.css'],
        footable: ['../node_modules/weblibs/footable/dist/footable.all.min.js',
            '../node_modules/weblibs/footable/css/footable.core.css'],
        fullcalendar: ['../node_modules/weblibs/moment/moment.js',
            '../node_modules/weblibs/fullcalendar/dist/fullcalendar.min.js',
            '../node_modules/weblibs/fullcalendar/dist/fullcalendar.css',
            '../node_modules/weblibs/fullcalendar/dist/fullcalendar.theme.css'],
        daterangepicker: ['../node_modules/weblibs/moment/moment.js',
            '../node_modules/weblibs/bootstrap-daterangepicker/daterangepicker.js',
            '../node_modules/weblibs/bootstrap-daterangepicker/daterangepicker-bs3.css'],
        tagsinput: ['../node_modules/weblibs/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js',
            '../node_modules/weblibs/bootstrap-tagsinput/dist/bootstrap-tagsinput.css']

    }
    )
    // oclazyload config
    .config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
        // We configure ocLazyLoad to use the lib script.js as the async loader
        $ocLazyLoadProvider.config({
            debug: true,
            events: true,
            modules: [
                {
                    name: 'ngGrid',
                    files: [
                        '../node_modules/weblibs/ng-grid/build/ng-grid.min.js',
                        '../node_modules/weblibs/ng-grid/build/ng-grid.min.css',
                        '../node_modules/weblibs/ng-grid/build/ng-grid.bootstrap.css'
                    ]
                },
                {
                    name: 'ui.grid',
                    files: [
                        '../node_modules/weblibs/angular-ui-grid/ui-grid.min.js',
                        '../node_modules/weblibs/angular-ui-grid/ui-grid.min.css',
                        '../node_modules/weblibs/angular-ui-grid/ui-grid.bootstrap.css'
                    ]
                },
                {
                    name: 'angularFileUpload',
                    files: [
                        '../node_modules/weblibs/angular-file-upload/angular-file-upload.min.js'
                    ]
                },
                {
                    name: 'ui.calendar',
                    files: ['../node_modules/weblibs/angular-ui-calendar/src/calendar.js']
                },
                {
                    name: 'ngImgCrop',
                    files: [
                        '../node_modules/weblibs/ngImgCrop/compile/minified/ng-img-crop.js',
                        '../node_modules/weblibs/ngImgCrop/compile/minified/ng-img-crop.css'
                    ]
                },
                {
                    name: 'angularBootstrapNavTree',
                    files: [
                        '../node_modules/weblibs/angular-bootstrap-nav-tree/dist/abn_tree_directive.js',
                        '../node_modules/weblibs/angular-bootstrap-nav-tree/dist/abn_tree.css'
                    ]
                },
                {
                    name: 'toaster',
                    files: [
                        '../node_modules/weblibs/angularjs-toaster/toaster.min.js',
                        '../node_modules/weblibs/angularjs-toaster/toaster.min.css'
                    ]
                },
                {
                    name: 'textAngular',
                    files: [
                        '../node_modules/weblibs/textAngular/dist/textAngular-sanitize.min.js',
                        '../node_modules/weblibs/textAngular/dist/textAngular.min.js'
                    ]
                },
                {
                    name: 'vr.directives.slider',
                    files: [
                        '../node_modules/weblibs/venturocket-angular-slider/build/angular-slider.min.js',
                        '../node_modules/weblibs/venturocket-angular-slider/build/angular-slider.css'
                    ]
                },
                {
                    name: 'com.2fdevs.videogular',
                    files: [
                        '../node_modules/weblibs/videogular/videogular.min.js'
                    ]
                },
                {
                    name: 'com.2fdevs.videogular.plugins.controls',
                    files: [
                        '../node_modules/weblibs/videogular-controls/controls.min.js'
                    ]
                },
                {
                    name: 'com.2fdevs.videogular.plugins.buffering',
                    files: [
                        '../node_modules/weblibs/videogular-buffering/buffering.min.js'
                    ]
                },
                {
                    name: 'com.2fdevs.videogular.plugins.overlayplay',
                    files: [
                        '../node_modules/weblibs/videogular-overlay-play/overlay-play.min.js'
                    ]
                },
                {
                    name: 'com.2fdevs.videogular.plugins.poster',
                    files: [
                        '../node_modules/weblibs/videogular-poster/poster.min.js'
                    ]
                },
                {
                    name: 'com.2fdevs.videogular.plugins.imaads',
                    files: [
                        '../node_modules/weblibs/videogular-ima-ads/ima-ads.min.js'
                    ]
                },
                {
                    name: 'xeditable',
                    files: [
                        '../node_modules/weblibs/angular-xeditable/dist/js/xeditable.min.js',
                        '../node_modules/weblibs/angular-xeditable/dist/css/xeditable.css'
                    ]
                },
                {
                    name: 'smart-table',
                    files: [
                        '../node_modules/weblibs/angular-smart-table/dist/smart-table.min.js'
                    ]
                }
            ]
        });
    }])
    ;
