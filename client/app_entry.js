
/** style css less */
require('./css/bootstrap/less/bootstrap.less');
require('./css/selectize/selectize.bootstrap3.less');
require('./css/app/less/app.less');

/** web logic */
require('./src/main.js');

