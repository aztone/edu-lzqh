var Sequelize = require('sequelize');

/** 全局库 */
exports.GLOBALTABLE = [
    'global_meta',
    'global_config',
    'level',
    'level_permission',
    'permission',
    'area_code',
    'area_street',
    'tenant'
];

/** 配置表定义 */
var SCHEMA = {};
SCHEMA.user = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    tenant_id: { type: Sequelize.INTEGER, allowNull: false },
    user_name: { type: Sequelize.STRING(50), allowNull: false, unique: true },
    name: { type: Sequelize.STRING(50), allowNull: false },
    sex: { type: Sequelize.STRING(1), allowNull: true },
    password: { type: Sequelize.STRING(32), allowNull: false },
    status: { type: Sequelize.INTEGER(4), allowNull: false },
    level: { type: Sequelize.INTEGER, allowNull: false },
    tel: { type: Sequelize.STRING(30), allowNull: false, unique: true },
    property: {type: Sequelize.INTEGER(4), allowNull: false},
    department_id: { type: Sequelize.STRING(200), allowNull: false },
    display_picture: {type: Sequelize.TEXT, allowNull: true},
    read_count: { type: Sequelize.INTEGER, allowNull: true },
    integral: { type: Sequelize.INTEGER, allowNull: true },
    card_id: { type: Sequelize.STRING(30), allowNull: true },
    identity: { type: Sequelize.STRING(30), allowNull: true },
    birthday: { type: Sequelize.DATE },
    address: { type: Sequelize.STRING(200), allowNull: true },
    email: { type: Sequelize.STRING(60), allowNull: true },
    post: { type: Sequelize.STRING(10), allowNull: true },
    degree: { type: Sequelize.STRING(50), allowNull: true },
    con_name: { type: Sequelize.STRING(50), allowNull: true },
    con_tel: { type: Sequelize.STRING(50), allowNull: true },
    remark: { type: Sequelize.STRING(250), allowNull: true },
    create_by: { type: Sequelize.INTEGER, allowNull: true },
    opt_by: { type: Sequelize.INTEGER, allowNull: true },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE },
    standby1: { type: Sequelize.STRING(500), allowNull: true },
    standby2: { type: Sequelize.STRING(500), allowNull: true },
    standby3: { type: Sequelize.STRING(500), allowNull: true }
};

SCHEMA.teachersubject = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    tenant_id: { type: Sequelize.INTEGER, allowNull: false },
    t_id: { type: Sequelize.INTEGER, allowNull: false },
    department: { type: Sequelize.INTEGER, allowNull: false },
    role: { type: Sequelize.STRING(5), allowNull: false },
    subject: { type: Sequelize.STRING(32), allowNull: false, unique: true },
    create_by: { type: Sequelize.INTEGER, allowNull: true },
    opt_by: { type: Sequelize.INTEGER, allowNull: true },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE },
    standby1: { type: Sequelize.STRING(500), allowNull: true },
    standby2: { type: Sequelize.STRING(500), allowNull: true },
    standby3: { type: Sequelize.STRING(500), allowNull: true }
};

SCHEMA.area_code = {
    code: { type: Sequelize.STRING(4), primaryKey: true },
    city: { type: Sequelize.STRING(50), allowNull: false }
};

SCHEMA.area_street = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    code: { type: Sequelize.STRING(11), allowNull: false },
    parent: { type: Sequelize.STRING(11), allowNull: false },
    name: { type: Sequelize.STRING(50), allowNull: false },
    level: { type: Sequelize.INTEGER(4), allowNull: false }
};

SCHEMA.global_config = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    key: { type: Sequelize.STRING(50), allowNull: false },
    context: { type: Sequelize.STRING(255), allowNull: false },
    value: { type: Sequelize.STRING(255), allowNull: false },
    status: { type: Sequelize.INTEGER(4), allowNull: false },
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE }
};

SCHEMA.global_meta = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    key: { type: Sequelize.STRING(50), allowNull: false },
    context: { type: Sequelize.STRING(50), allowNull: false },
    value: { type: Sequelize.TEXT, allowNull: false },
    status: { type: Sequelize.INTEGER(4), allowNull: false },
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE }
};

SCHEMA.level = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    level: { type: Sequelize.INTEGER, unique: true, allowNull: false },
    name: { type: Sequelize.STRING(50), allowNull: false },
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE }
};

SCHEMA.level_permission = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    level_id: { type: Sequelize.INTEGER, allowNull: false },
    permission_id: { type: Sequelize.INTEGER, allowNull: false },
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE }
};

SCHEMA.permission = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    permission: { type: Sequelize.STRING(50), unique: true, allowNull: false },
    name: { type: Sequelize.STRING(50), allowNull: false },
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE }
};

SCHEMA.customer = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    tenant_id: { type: Sequelize.INTEGER, allowNull: false },
    name: { type: Sequelize.STRING(50), allowNull: false },
    tel: { type: Sequelize.STRING(20), allowNull: false },
    email: { type: Sequelize.STRING(20), allowNull: true },
    qq: { type: Sequelize.STRING(20), allowNull: true },
    street: { type: Sequelize.STRING(11), allowNull: true },
    address: { type: Sequelize.STRING(255), allowNull: true },
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE }
};

SCHEMA.department = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    tenant_id: { type: Sequelize.INTEGER, allowNull: false },
    name: { type: Sequelize.STRING(50), allowNull: false },
    parent: { type: Sequelize.INTEGER, allowNull: true },
    ancestors: { type: Sequelize.STRING, allowNull: true },
    depth: { type: Sequelize.INTEGER(4), allowNull: false },
    dep_code: { type: Sequelize.STRING(30), allowNull: true },
    status: { type: Sequelize.INTEGER(4), allowNull: false },
    property: {type: Sequelize.INTEGER(4), allowNull: true},
    con_name: { type: Sequelize.STRING(50), allowNull: true },
    con_tel: { type: Sequelize.STRING(50), allowNull: true },
    remark: { type: Sequelize.STRING(250), allowNull: true },
    create_by: { type: Sequelize.INTEGER, allowNull: true },
    opt_by: { type: Sequelize.INTEGER, allowNull: true },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE },
    standby1: { type: Sequelize.STRING(500), allowNull: true },
    standby2: { type: Sequelize.STRING(500), allowNull: true },
    standby3: { type: Sequelize.STRING(500), allowNull: true }
};

SCHEMA.config = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    tenant_id: { type: Sequelize.INTEGER, allowNull: false },
    key: { type: Sequelize.STRING(50), allowNull: false },
    context: { type: Sequelize.STRING(255), allowNull: false },
    value: { type: Sequelize.STRING(255), allowNull: false },
    status: { type: Sequelize.INTEGER(4), allowNull: false },
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE }
};

SCHEMA.metadata = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    tenant_id: { type: Sequelize.INTEGER, allowNull: false },
    key: { type: Sequelize.STRING(50), allowNull: false },
    context: { type: Sequelize.STRING(50), allowNull: false },
    value: { type: Sequelize.TEXT, allowNull: false },
    status: { type: Sequelize.INTEGER(4), allowNull: false },
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE }
};

SCHEMA.tenant = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    name: { type: Sequelize.STRING(50), allowNull: false, unique: true },
    alias: { type: Sequelize.STRING(50), allowNull: false },
    status: { type: Sequelize.INTEGER(4), allowNull: false },
    opt_by: { type: Sequelize.INTEGER, allowNull: false },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE }
};

SCHEMA.books = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    tenant_id: { type: Sequelize.INTEGER, allowNull: false },
    name: { type: Sequelize.STRING(100), allowNull: false },
    picture: {type: Sequelize.TEXT, allowNull: true},
    author: { type: Sequelize.STRING(30), allowNull: true },
    authoring_ways: { type: Sequelize.STRING(30), allowNull: true },
    book_type: { type: Sequelize.STRING(200), allowNull: true },
    recommended_language: { type: Sequelize.STRING(200), allowNull: true },
    description: { type: Sequelize.STRING(200), allowNull: true },
    introduction: { type: Sequelize.STRING(500), allowNull: true },
    appreciation: {type: Sequelize.TEXT, allowNull: true},
    press: { type: Sequelize.STRING(60), allowNull: true },
    recommended_count: { type: Sequelize.INTEGER, allowNull: true },
    publish_at: { type: Sequelize.DATE },
    status: { type: Sequelize.INTEGER(4), allowNull: false },
    integral: { type: Sequelize.INTEGER, allowNull: true },
    remark: { type: Sequelize.STRING(250), allowNull: true },
    create_by: { type: Sequelize.INTEGER, allowNull: true },
    opt_by: { type: Sequelize.INTEGER, allowNull: true },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE },
    standby1: { type: Sequelize.STRING(500), allowNull: true },
    standby2: { type: Sequelize.STRING(500), allowNull: true },
    standby3: { type: Sequelize.STRING(500), allowNull: true },
    standby4: { type: Sequelize.STRING(500), allowNull: true },
    standby5: { type: Sequelize.STRING(500), allowNull: true }
};

SCHEMA.booklist = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    tenant_id: { type: Sequelize.INTEGER, allowNull: false },
    book_id: { type: Sequelize.INTEGER, allowNull: false },
    booklist_type: { type: Sequelize.STRING(200), allowNull: true },
    period: { type: Sequelize.STRING(200), allowNull: true },
    limit_time: { type: Sequelize.INTEGER(3), allowNull: false },
    status: { type: Sequelize.INTEGER(4), allowNull: false },
    integral: { type: Sequelize.INTEGER, allowNull: true },
    remark: { type: Sequelize.STRING(250), allowNull: true },
    create_by: { type: Sequelize.INTEGER, allowNull: true },
    opt_by: { type: Sequelize.INTEGER, allowNull: true },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE },
    standby1: { type: Sequelize.STRING(500), allowNull: true },
    standby2: { type: Sequelize.STRING(500), allowNull: true },
    standby3: { type: Sequelize.STRING(500), allowNull: true },
    standby4: { type: Sequelize.STRING(500), allowNull: true },
    standby5: { type: Sequelize.STRING(500), allowNull: true }
};

SCHEMA.bookshelves = {
    id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
    tenant_id: { type: Sequelize.INTEGER, allowNull: false },
    booklist_id: { type: Sequelize.INTEGER, allowNull: false },
    book_id: { type: Sequelize.INTEGER, allowNull: false },
    own_by: { type: Sequelize.INTEGER, allowNull: false },
    source: { type: Sequelize.INTEGER, allowNull: false },
    status: { type: Sequelize.INTEGER(4), allowNull: false },
    assess_count: { type: Sequelize.INTEGER(4), allowNull: false },
    remark: { type: Sequelize.STRING(250), allowNull: true },
    create_by: { type: Sequelize.INTEGER, allowNull: true },
    opt_by: { type: Sequelize.INTEGER, allowNull: true },
    create_at: { type: Sequelize.DATE },
    update_at: { type: Sequelize.DATE },
    standby1: { type: Sequelize.STRING(500), allowNull: true },
    standby2: { type: Sequelize.STRING(500), allowNull: true },
    standby3: { type: Sequelize.STRING(500), allowNull: true },
    standby4: { type: Sequelize.STRING(500), allowNull: true },
    standby5: { type: Sequelize.STRING(500), allowNull: true }
};

exports.SCHEMA = SCHEMA;