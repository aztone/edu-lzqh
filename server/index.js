/**
 * @fileOverview 路由
 * @author wing ying_gong@bjdv.com
 */

//var logger = require('log4js').getLogger('index');

var Dbl = require('./db').Dbl;
module.exports = function (app) {
    /** http */
    app.all('/*', function (req, res, next) {
        req.param = function (key) {
            return req.params[key] || req.body[key] || req.query[key];
        };
        next();
    });
    app.use('/access', require('./routers/access'));

    /** 授权 */
    app.all('/api/*', function (req, res, next) {
        if (!req.user || !req.user.tenant_id) {
            var e = new Error('租户未授权请重新登录');
            e.status = 401;
            throw e;
        } else {
            req.dbl = new Dbl(req.user.tenant_id, req.user.id);
            next();
        }
    });
    app.use('/api/info', function (req, res) {
        res.json({ result: req.user || '' });
    });

    /** 数据库操作层 */
    /** 全局 */
    app.use('/api/level', require('./routers/global/level'));
    app.use('/api/level_permission', require('./routers/global/level_permission'));
    app.use('/api/permission', require('./routers/global/permission'));
    app.use('/api/global_meta', require('./routers/global/global_meta'));
    app.use('/api/global_config', require('./routers/global/global_config'));
    app.use('/api/area_street', require('./routers/global/area_street'));

    /** 租户 */
    app.use('/api/tenant', require('./routers/global/tenant'));
    app.use('/api/metadata', require('./routers/system/metadata'));
    app.use('/api/config', require('./routers/system/config'));
    app.use('/api/user', require('./routers/system/user'));
    app.use('/api/department', require('./routers/system/department'));
    app.use('/api/teachersubject', require('./routers/system/teachersubject'));

    /** 图书 */
    app.use('/api/books', require('./routers/book/books'));
    app.use('/api/booklist', require('./routers/book/booklist'));
    app.use('/api/bookshelves', require('./routers/book/bookshelves'));


    /** 日常 */
    app.use('/api/customer', require('./routers/business/customer'));

    /** 其他 */
    app.use('/api/import', require('./routers/import'));
    app.use('/api/file', require('./routers/resource/file'));   
   
};