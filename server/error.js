exports.error = (o) => {
    if (!o) {
        o = exports.CAUSE.UNKNOWN;
    }
    var err = new Error(o.message);
    err.cuase = o.cuase;
    return err;
};

exports.CAUSE = {
    UNKNOWN: { cuase: 800, message: '未知异常' },
    TABLE_NOTEXIST: { cuase: 801, message: '表不存在' },
    DATA_ERR: {cuase: 802, message: '数据有误'},
    USER_NOTEXIST: {cuase: 803, message: '认证用户不存在'},
    USER_DISABLED: {cuase: 804, message: '认证用户已停用'},
    USER_PASSWORD_ERR: {cuase: 805, message: '认证密码有误'},
    USER_EXPIRE_ERR: {cuase: 806, message: '认证过期时间有误'},
    DEPARTMENT_PARENT_ERR: {cuase: 820, message: '上级部门有误'},
    DEPARTMENT_PARENT_CANNOT_MODIFY: {cuase:821, message: '根部门不能修改'},
    DEPARTMENT_CANNOT_BELONG_SELF_SON: {cuase: 822, message: '部门不能挂在自己或者子部门下'},
    DEPARTMENT_CANNOT_DELETE_HAS_SON: {cuase:823, message: '不能删除还有子部门的部门'},
    DEPARTMENT_ERR: {cuase: 824, message:'部门id有误'},
    INCTABLE_DATE_ERR: {cuase: 825, message:'增量库日期有误'}
};