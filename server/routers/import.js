'use strict';
var Promise = require('bluebird');
var fs = require('fs');
var path = require('path');
var logger = require('log4js').getLogger('import');
var express = require('express');
var iconv = require('iconv-lite');
var router = express.Router();

module.exports = router;

var UPLOADS_PATH = path.join(__dirname, '../../uploads');

router.get('/:file', function (req, res) {
    var file = req.param('file');
    fs.readFile(path.join(UPLOADS_PATH, file), function (err, content) {
        if (!err) {
            content = iconv.decode(content, 'gbk');
            res.json({ result: content });
        } else {
            res.json({err: err.message});
        }
    });
});
