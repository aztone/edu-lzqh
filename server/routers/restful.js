/**
 * @fileOverview restful公用api
 * @author wing ying_gong@bjdv.com
 */
var express = require('express');
var Promise = require('bluebird');
var _ = require('lodash');

var routers = {};

/** restful基础配置路由 */
module.exports = function (tableName, router, methods = ['findOne', 'findAll', 'count', 'create', 'update', 'save', 'upsert', 'remove']) {
    if (!routers[tableName]) {
        if (!router) {
            router = express.Router();
        }
        var logger = require('log4js').getLogger(tableName);

        if (_.includes(methods, 'findAll')) {
            router.get('/', function (req, res) {
                var page = req.query.page;
                var limit = req.query.limit;
                var options = req.query.options;
                Promise.resolve().then(function () {
                    if (_.isUndefined(limit)) {
                        return req.dbl.findAll(tableName, options);
                    } else {
                        return req.dbl.findAllPage(tableName, options, page, limit);
                    }
                }).then(function (data) {
                    res.json({ result: data });
                }).catch(function (e) {
                    res.json({err: e.res||800, result: e.message });
                    logger.error(e.stack);
                });
            });
        }

        if (_.includes(methods, 'count')) {
            router.get('/count', function (req, res) {
                var options = req.query.options;
                Promise.resolve().then(function () {
                    return req.dbl.count(tableName, options);
                }).then(function (count) {
                    res.json({ result: count });
                }).catch(function (e) {
                    res.json({err: e.res||800, result: e.message });
                    logger.error(e.stack);
                });
            });
        }

        if (_.includes(methods, 'findOne')) {
            router.get('/:id', function (req, res) {
                var id = req.params.id;
                var options = req.query.options;
                Promise.resolve().then(function () {
                    return req.dbl.findOne(tableName, id, options);
                }).then(function (data) {
                    res.json({ result: data });
                }).catch(function (e) {
                    res.json({err: e.res||800, result: e.message });
                    logger.error(e.stack);
                });
            });
        }

        if (_.includes(methods, 'create')) {
            router.post('/', function (req, res) {
                var data = req.body.data;
                var where = req.body.where;
                Promise.resolve().then(function () {
                    if (!_.isUndefined(where)) {
                        return req.dbl.upsert(tableName, data, where);
                    } else {
                        return req.dbl.create(tableName, data);
                    }

                }).then(function () {
                    res.json({ result: true });
                }).catch(function (e) {
                    res.json({err: e.res||800, result: e.message });
                    logger.error(e.stack);
                });
            });
        }

        if (_.includes(methods, 'update')) {
            router.put('/', function (req, res) {
                var where = req.body.where;
                var data = req.body.data;
                Promise.resolve().then(function () {
                    return req.dbl.update(tableName, data, where);
                }).then(function () {
                    res.json({ result: true });
                }).catch(function (e) {
                    res.json({err: e.res||800, result: e.message });
                    logger.error(e.stack);
                });
            });
        }

        if (_.includes(methods, 'save')) {
            router.put('/:id', function (req, res) {
                var id = req.params.id;
                var data = req.body.data;
                Promise.resolve().then(function () {
                    return req.dbl.update(tableName, data, {id: id});
                }).then(function () {
                    res.json({ result: true });
                }).catch(function (e) {
                    res.json({err: e.res||800, result: e.message });
                    logger.error(e.stack);
                });
            });
        }

        if (_.includes(methods, 'remove')) {
            router.delete('/:id', function (req, res) {
                var id = req.params.id;
                Promise.resolve().then(function () {
                    return req.dbl.remove(tableName, id);
                }).then(function () {
                    res.json({ result: true });
                }).catch(function (e) {
                    res.json({err: e.res||800, result: e.message });
                    logger.error(e.stack);
                });
            });
        }
        routers[tableName] = router;
    } else {
        router = routers[tableName];
    }
    return router;
};