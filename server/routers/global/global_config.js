var logger = require('log4js').getLogger('global_config');
var guard = require('express-jwt-permissions')();
var express = require('express');
var router = express.Router();
var CATEGORIES = [
    { key: 'test', name: '测试配置' }
];
/*
router.get('*', guard.check('global:read'));
router.post('*', guard.check('global:write'));
router.put('*', guard.check('global:write'));
router.delete('*', guard.check('global:write'));*/
router.get('/category', function (req, res) {
    res.json({ result: CATEGORIES });
});

module.exports = require('../restful')('global_config', router);