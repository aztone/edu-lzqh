var express = require('express');
var router = express.Router();
var logger = require('log4js').getLogger('level');
var _ = require('lodash');
var Promise = require('bluebird');

router.post('/', function (req, res) {
    var data = req.body.data;
    var level;
    Promise.resolve().then(function () {
        return req.dbl.create('level', data);
    }).then(function (row) {
        level = row;
        return data.permissions;
    }).each(function (id) {
        return req.dbl.create('level_permission', {level_id: level.id, permission_id: id});
    }).then(function () {
        res.json({ result: true });
    }).catch(function (e) {
        res.json({ err: e.cause || 800, result: e.message });
        logger.error(e.stack);
    });
});

router.put('/:id', function (req, res) {
    var id = req.params.id;
    var data = req.body.data;
    Promise.resolve().then(function () {
        return req.dbl.update('level', data, { id: id });
    }).then(function () {
        return req.dbl.findAll('level_permission', {where: {level_id: id}, attributes: ['id']});
    }).then(function (rows) {
        return _.map(rows, 'id');
    }).each(function (lpId) {
        return req.dbl.remove('level_permission', lpId);
    }).then(function () {
        return data.permissions;
    }).each(function (permissionId) {
        return req.dbl.create('level_permission', {level_id: id, permission_id: permissionId});
    }).then(function () {
        res.json({ result: true });
    }).catch(function (e) {
        res.json({ err: e.cause || 800, result: e.message });
        logger.error(e.stack);
    });
});

module.exports = require('../restful')('level', router);