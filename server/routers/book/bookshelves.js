
var express = require('express');
var router = express.Router();

var Promise = require('bluebird');
var _ = require('lodash');
var logger = require('log4js').getLogger('bookshelves');

module.exports = require('../restful')('bookshelves', router);