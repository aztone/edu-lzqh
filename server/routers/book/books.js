
var express = require('express');
var router = express.Router();

var Promise = require('bluebird');
var _ = require('lodash');
var logger = require('log4js').getLogger('books');

module.exports = require('../restful')('books', router);