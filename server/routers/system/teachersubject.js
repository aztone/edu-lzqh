
var express = require('express');
var router = express.Router();

var Promise = require('bluebird');
var _ = require('lodash');
var logger = require('log4js').getLogger('teachersubject');

module.exports = require('../restful')('teachersubject', router);